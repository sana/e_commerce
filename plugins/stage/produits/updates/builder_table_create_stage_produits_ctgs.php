<?php namespace Stage\Produits\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateStageProduitsCtgs extends Migration
{
    public function up()
    {
        Schema::create('stage_produits_ctgs', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('categorie');
            $table->string('slug');
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('stage_produits_ctgs');
    }
}
