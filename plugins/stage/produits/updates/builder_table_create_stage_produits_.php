<?php namespace Stage\Produits\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateStageProduits extends Migration
{
    public function up()
    {
        Schema::create('stage_produits_', function($table)
        {
            $table->engine = 'InnoDB';
            $table->string('sku');
            $table->string('nom');
            $table->text('description')->nullable();
            $table->string('image')->nullable();
            $table->double('prix', 10, 0);
            $table->integer('quantite_en_stock')->unsigned();
            $table->primary(['sku']);
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('stage_produits_');
    }
}
