<?php namespace Stage\Produits\Models;

use Model;

/**
 * Model
 */
class Ctg extends Model
{
    use \October\Rain\Database\Traits\Validation;
    
    /*
     * Disable timestamps by default.
     * Remove this line if timestamps are defined in the database table.
     */
    public $timestamps = false;

    /**
     * @var array Validation rules
     */
    public $rules = [
    ];

    /**
     * @var string The database table used by the model.
     */
    public $table = 'stage_produits_ctgs';

public $belongsToMany =[ 'produits'=>[ 'stage\Produits\Models\Produit', 'table' => 'stage_produits_appartenir_ctgs','order' => 'nom']];}
